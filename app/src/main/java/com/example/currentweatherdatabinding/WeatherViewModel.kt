package com.example.currentweatherdatabinding

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WeatherViewModel : ViewModel() {
    val weather = MutableLiveData<WeatherViewData>()

    fun newWeather(weatherViewData: WeatherViewData){
        weather.postValue(weatherViewData)
    }
}