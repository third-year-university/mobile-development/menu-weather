package com.example.currentweatherdatabinding

import android.content.Context
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date


class WeatherViewData() {
    companion object{
        val imgIds = mapOf(
            Pair("01d", R.drawable.i01d),
            Pair("02d", R.drawable.i02d),
            Pair("03d", R.drawable.i03d),
            Pair("04d", R.drawable.i04d),
            Pair("09d", R.drawable.i09d),
            Pair("10d", R.drawable.i10d),
            Pair("11d", R.drawable.i11d),
            Pair("13d", R.drawable.i13d),
            Pair("50d", R.drawable.i50d),
            Pair("01n", R.drawable.i01n),
            Pair("02n", R.drawable.i02n),
            Pair("03n", R.drawable.i03n),
            Pair("04n", R.drawable.i04n),
            Pair("09n", R.drawable.i09n),
            Pair("10n", R.drawable.i10n),
            Pair("11n", R.drawable.i11n),
            Pair("13n", R.drawable.i13n),
            Pair("50n", R.drawable.i50n)
            )
        val windIcons = arrayOf(
            R.drawable.w0,
            R.drawable.w30,
            R.drawable.w45,
            R.drawable.w60,
            R.drawable.w90,
            R.drawable.w120,
            R.drawable.w120,
            R.drawable.w135,
            R.drawable.w150,
            R.drawable.w180,
            R.drawable.w210,
            R.drawable.w240,
            R.drawable.w270,
            R.drawable.w300,
            R.drawable.w315,
            R.drawable.w330
            )
        const val NONE = -999.0
    }

    constructor(weatherData: WeatherData, context: Context) : this() {
        isInitialized = true
        windDegrees = weatherData.wind?.deg!!
        weatherIcon = weatherData.weather[0].icon!!
        temperature = weatherData.main?.temp!!
        humidity = weatherData.main!!.humidity!!.toDouble()
        city = weatherData.name.toString()
        text = "$city\n ${context.getString(R.string.temperature)}: $temperature°C\n" +
                "${context.getString(R.string.humidity)}: $humidity%"
        val sunrise = Date(weatherData.sys?.sunrise?.times(1000L)!!)
        val sunset = Date(weatherData.sys?.sunset?.times(1000L)!!)
        val fmt: DateFormat = SimpleDateFormat("HH:mm")
        textDetailed = "$city\n" +
                "${context.getString(R.string.temperature)}: $temperature°C\n" +
                "${context.getString(R.string.humidity)}: $humidity%\n" +
                "${context.getString(R.string.wind)}: $windDegrees°, ${weatherData.wind!!.speed} ${context.getString(
                            R.string.m_s)}\n" +
                "${context.getString(R.string.condition)}: ${weatherData.weather[0].description}\n" +
                "${context.getString(R.string.sunrise)}: ${fmt.format(sunrise)} ${context.getString(
                            R.string.utc)}, ${context.getString(R.string.sunset)}: ${fmt.format(sunset)} ${context.getString(
                    R.string.utc)}"

    }

    var isInitialized = false
    var iconID: Int = 0
    var windIconID: Int = 0

    var windDegrees: Int = 0
        set(value){
            field = value
            windIconID = windIcons[0]
            for (i in 1..15){
                if (value <= (i + 1) * 22.5 + 11.25 && value >= i * 22.5 - 11.25){
                    windIconID = windIcons[i]
                }
            }
        }
    var weatherIcon: String = ""
        set(value) {
            field = value
            iconID = if (!imgIds.containsKey(value)){
                R.drawable.none
            } else{
                imgIds[value]!!
            }
        }

    private var temperature = NONE
    private var humidity = NONE
    private var city = ""
    var text = ""
    var textDetailed = ""
}