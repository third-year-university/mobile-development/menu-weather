package com.example.currentweatherdatabinding

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class MyDialog(val ctx: Context, val fm: FragmentManager, val fr1: Fragment, val fr2: Fragment): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val options = arrayOf(getString(R.string.short_weather_report), getString(R.string.detailed_weather_report))
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(getString(R.string.choose_weather_report_design))
                .setItems(options
                ) { dialog, which ->
                    val ft = fm.beginTransaction()
                    if (which == 0){
                        ft.replace(R.id.frame_layout, fr1)
                    }
                    if (which == 1){
                        ft.replace(R.id.frame_layout, fr2)
                    }
                    ft.commit()
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}