package com.example.currentweatherdatabinding

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.currentweatherdatabinding.databinding.FragmentDetailedWeatherBinding



class DetailedWeatherFragment() : Fragment() {
    private lateinit var binding: FragmentDetailedWeatherBinding

    lateinit var viewModel: WeatherViewModel
    lateinit var weatherViewData: WeatherViewData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider[WeatherViewModel::class.java]

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detailed_weather, container, false)
        viewModel.weather.observe(viewLifecycleOwner, Observer {
            binding.weather = it
        })

        return binding.root
    }

    fun receiveWeather(weatherViewData: WeatherViewData){
        binding.weather = weatherViewData
        viewModel.newWeather(weatherViewData)
        this.weatherViewData = weatherViewData
    }
}