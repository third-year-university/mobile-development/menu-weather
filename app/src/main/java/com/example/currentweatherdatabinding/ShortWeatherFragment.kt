package com.example.currentweatherdatabinding

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.currentweatherdatabinding.databinding.FragmentShortWeatherBinding


class ShortWeatherFragment() : Fragment() {

    private lateinit var binding: FragmentShortWeatherBinding

    lateinit var viewModel: WeatherViewModel
    lateinit var weatherViewData: WeatherViewData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider[WeatherViewModel::class.java]

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_short_weather, container, false)
        viewModel.weather.observe(viewLifecycleOwner, Observer {
            binding.weather = it
        })

        return binding.root
    }

    fun receiveWeather(weatherViewData: WeatherViewData){
        binding.weather = weatherViewData
        viewModel.newWeather(weatherViewData)
        this.weatherViewData = weatherViewData
    }

}